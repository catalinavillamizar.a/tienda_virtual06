package com.example.TiendaVirtual;

import com.example.TiendaVirtual.modelos.Inventario;
import com.example.TiendaVirtual.modelos.Productovendido;
import com.example.TiendaVirtual.modelos.Usuario;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@SpringBootApplication // Indica que es la clase Main
@RestController // Indica que la clase será un API REST. HTTPS

public class TiendaVirtualSpringBootApplication {
    
    @Autowired(required = true)
    Inventario i;    
    @Autowired(required = true)
    Productovendido p;
    @Autowired(required = true)
    Usuario u;

    
    public static void main(String[] args) {
        SpringApplication.run(TiendaVirtualSpringBootApplication.class, args);
    }
 
    @GetMapping("/HomeTiendita")
    public String HomeTienditaMamalona(@RequestParam(value = "Name", defaultValue = "DonPepe") String Name,
            @RequestParam(value = "Fecha", defaultValue = "1907") int Fecha) {
        return String.format("Bienvenido %s a su tienda     _:%s:_", Name, Fecha);
    }
    
    @GetMapping("/Inventario")
    public String consultarInventarioPorid_usuario(@RequestParam(value = "id_producto", defaultValue = "Porfavor Coloque el id del producto") int id_producto) throws ClassNotFoundException, SQLException {
        i.setid_producto(id_producto);
        if (i.consultarProducto()) { // True
            String res = new Gson().toJson(i);
            i.setid_producto(0); // Si hay más registros SOLO traigase el primero que encuentre
            i.setnombre_producto("");
            i.setfecha_venta(0);
            i.setprocedencia("");
            i.setprecio(0);
            i.setcantidad(0);
            i.settipo_almacenamiento("");
            i.setid_usuario(0);
            return res;
        } else { // False           
           return new Gson().toJson(i);
        }
    }
    
    // GET - POST - PUT - DELETE
    @PostMapping(path = "/Inventario", // le enviamos datos al servidor
        consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
        produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  => {nombre = Carlos}
   
    
    public String actualizarInventario(@RequestBody String Inventario) throws ClassNotFoundException, SQLException{
        Inventario f = new Gson().fromJson(Inventario, Inventario.class); // recibimos el json y lo devolvemos un objeto estudiante
        i.setid_producto(f.getid_producto()); // obtengo el id de f y se lo ingreso e
        i.setfecha_venta(f.getfecha_venta());
        i.setprecio(f.getprecio());
        i.setcantidad(f.getcantidad());
        i.actualizar();
        return new Gson().toJson(i); // cliente le envia json al servidor. fpr de comunicarse
        
    }
    
    
    @DeleteMapping("/eliminarcuenta/{id_producto}")
    public String borrar(@PathVariable("id_producto") int id_producto) throws SQLException, ClassNotFoundException{
        i.setid_producto(id_producto);
        i.borrar();
        return "Los datos del producto indicado han sido eliminados";
    }
}
    
    