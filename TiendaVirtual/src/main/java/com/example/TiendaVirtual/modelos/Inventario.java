/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

package com.example.TiendaVirtual.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


/**
 *
 * @author Grupo 6 
 */


@Component("Inventario")
public class Inventario{
    
    
    
    // Este es el paquete que ejecuta las consultas
    @Autowired
    transient JdbcTemplate JdbcTemplate;    
    
    
    
    //atributos 
    private int id_producto;    
    private String nombre_producto;    
    private int fecha_venta;
    private String procedencia;
    private int precio;    
    private int cantidad;
    private int fecha_vencimiento;
    private String tipo_almacenamiento;    
    private int id_usuario;    
      
        
    
    //Constructor

    public Inventario(int id_producto, String nombre_producto, int fecha_venta, String procedencia, int precio, int cantidad, int fecha_vencimiento, String tipo_almacenamiento, int id_usuario) {
        this.id_producto = id_producto;
        this.nombre_producto = nombre_producto;
        this.fecha_venta = fecha_venta;
        this.procedencia = procedencia;
        this.precio = precio;
        this.cantidad = cantidad;
        this.fecha_vencimiento = fecha_vencimiento;
        this.tipo_almacenamiento = tipo_almacenamiento;
        this.id_usuario = id_usuario;
    }
    public Inventario() {
    }
   
    
    
    //Metodos
    public JdbcTemplate getJdbcTemplate() {
        return JdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate JdbcTemplate) {
        this.JdbcTemplate = JdbcTemplate;
    }

    public int getid_producto() {
        return id_producto;
    }

    public void setid_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getnombre_producto() {
        return nombre_producto;
    }

    public void setnombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public int getfecha_venta() {
        return fecha_venta;
    }

    public void setfecha_venta(int fecha_venta) {
        this.fecha_venta = fecha_venta;
    }

    public String getprocedencia() {
        return procedencia;
    }

    public void setprocedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public int getprecio() {
        return precio;
    }

    public void setprecio(int precio) {
        this.precio = precio;
    }

    public int getcantidad() {
        return cantidad;
    }

    public void setcantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getfecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setfecha_vencimiento(int fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String gettipo_almacenamiento() {
        return tipo_almacenamiento;
    }

    public void settipo_almacenamiento(String tipo_almacenamiento) {
        this.tipo_almacenamiento = tipo_almacenamiento;
    }

    public int getid_usuario() {
        return id_usuario;
    }

    public void setid_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }
        
    
    
    //Crud
        //guardar algo en la base de datos
    
    public String guardar() throws ClassNotFoundException , SQLException{
        String sql = "INSERT INTO inventario(id_producto, nombre_producto, fecha_venta, procedencia, precio, cantidad, fecha_vencimiento, tipo_almacenamiento, id_usuario) VALUES(?,?,?,?,?,?,?,?,?)";
        return sql;
    } 
     
    
    
    //Crud
        //Consultar o arrobart algo de la base de datos
    public boolean consultarProducto() throws ClassNotFoundException , SQLException{
        String sql = "SELECT id_producto, nombre_producto, fecha_venta, procedencia, precio, cantidad, fecha_vencimiento, tipo_almacenamiento, id_usuario FROM inventario WHERE id_producto = ?";
        List<Inventario> inventarios = JdbcTemplate.query(sql,(rs,rowNum)
            ->new Inventario(
                rs.getInt("id_producto"),
                rs.getString("nombre_producto"),
                rs.getInt("fecha_venta"),
                rs.getString("procedencia"),
                rs.getInt("precio"),
                rs.getInt("cantidad"), 
                rs.getInt("fecha_vencimiento"), 
                rs.getString("tipo_almacenamiento"), 
                rs.getInt("id_usuario") 
                
            ),  new Object[]{this.getid_producto()});
        
        if (inventarios != null && !inventarios.isEmpty()){
            this.setid_producto(inventarios.get(0).getid_producto());
            this.setnombre_producto(inventarios.get(0).getnombre_producto());
            this.setfecha_venta(inventarios.get(0).getfecha_venta());
            this.setprocedencia(inventarios.get(0).getprocedencia());
            this.setprecio(inventarios.get(0).getprecio());
            this.setcantidad(inventarios.get(0).getcantidad());
            this.setfecha_vencimiento(inventarios.get(0).getfecha_vencimiento());
            this.settipo_almacenamiento(inventarios.get(0).gettipo_almacenamiento());
            this.setid_usuario(inventarios.get(0).getid_usuario());
                        
            return true;
        }
        else{
            return false;
        }
    }
     
    public List<Inventario> ConsultarTodo(int id_producto) throws ClassNotFoundException , SQLException{ 
        String sql = "SELECT id_producto, nombre_producto, fecha_venta, procedencia, precio, cantidad, fecha_vencimiento, tipo_almacenamiento, id_usuario FROM inventario WHERE id_producto = ?";
        List<Inventario> inventarios = JdbcTemplate.query(sql,(rs,rowNum)
            ->new Inventario(
                rs.getInt("id_producto"),
                rs.getString("nombre_producto"),
                rs.getInt("fecha_venta"),
                rs.getString("procedencia"),
                rs.getInt("precio"),
                rs.getInt("cantidad"), 
                rs.getInt("fecha_vencimiento"), 
                rs.getString("tipo_almacenamiento"), 
                rs.getInt("id_usuario") 
                
            ),  new Object[]{this.getid_producto()});
        return inventarios;
    }
    
    
    
    
    //crud
        //actualizar
    public String actualizar() throws ClassNotFoundException , SQLException{
        String sql = "id_producto = ?, nombre_producto = ?, fecha_venta = ?, procedencia = ?, precio = ?, cantidad = ?, fecha_vencimiento = ?, tipo_almacenamiento = ?, id_usuario = ? FROM inventario WHERE id_producto = ?";
        return sql;
    }   
    
    
    //Crud
        //borrar
    public boolean borrar() throws ClassNotFoundException , SQLException{
        String sql = "DELETE FROM inventario WHERE id_producto = ?";
        Connection c = JdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getid_producto());
        ps.execute();
        ps.close();
        return true;
    }
   
}