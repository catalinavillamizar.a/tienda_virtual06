/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */



package com.example.TiendaVirtual.modelos;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


/**
 *
 * @author Grupo 6 
 */



@Component("Productovendido")
public class Productovendido{
    
    
    
    // Este es el paquete que ejecuta las consultas
    @Autowired
    transient JdbcTemplate JdbcTemplate;    
    
    
    
    //atributos 
    private int idproductos_vendidos;    
    private int cantidad;   
    
    
    
    //Constructor
    public Productovendido(int idproductos_vendidos, int cantidad) {
        this.idproductos_vendidos = idproductos_vendidos;
        this.cantidad = cantidad;
    }
    public Productovendido() {
    }
    
    
    
    //Metodos

    public JdbcTemplate getJdbcTemplate() {
        return JdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate JdbcTemplate) {
        this.JdbcTemplate = JdbcTemplate;
    }

    public int getIdproductos_vendidos() {
        return idproductos_vendidos;
    }

    public void setIdproductos_vendidos(int idproductos_vendidos) {
        this.idproductos_vendidos = idproductos_vendidos;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}    