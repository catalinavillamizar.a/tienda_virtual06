// JavaScript source code
angular.module('tiendavirtual',[])
.controller('filasInventario',function($scope,$http){
    
    $scope.nombre_producto = "";
    $scope.fecha_compra = "";
    $scope.procedencia = "";
    $scope.precio= 0;
    $scope.cantidad = 0;
    $scope.fecha_vencimiento = "";
    $scope.tipo_almacenamiento = "";
    
    // accion del boton consultar
    $scope.consultar = function(){
        if($scope.id_producto == undefined || $scope.id_producto == null){
            $scope.id_producto = 0;
        }
        
        $http.get("/tiendavirtual/inventario"+$scope.id_producto).then(function(data){
            console.log(data.data);

            $scope.nombre_producto = data.data.nombre_producto;
            $scope.fecha_compra = data.data.fecha_compra;
            $scope.procedencia = data.data.procedencia;
            $scope.precio= data.data.precio;
            $scope.cantidad = data.data.cantidad;
            $scope.fecha_vencimiento = data.data.fecha_vencimiento;
            $scope.tipo_almacenamiento = data.data.tipoalmacenamiento;
            
        },function(){
             //error
            $scope.nombre_producto = "";
            $scope.fecha_compra = "";
            $scope.procedencia = "";
            $scope.precio= 0;
            $scope.cantidad = 0;
            $scope.fecha_vencimiento = "";
            $scope.tipo_almacenamiento = "";
            $scope.filas = []; // guarda
        });
        
        $http.get("/tiendavirtual/inventario"+$scope.id_producto).then(function(data){
            console.log(data.data);
            $scope.filas = data.data;  
        },function(){
            $scope.filas = [];
        });
    };
    
    //acción del botón actualizar
    $scope.actualizar = function(){
        if($scope.id_producto == undefined || $scope.id_producto == null){
            $scope.id_producto = 0;
        }
        data = {
            "id_producto": $scope.id_producto,
            "nombre_producto":$scope.nombre_producto,
            "cantidad": $scope.cantidad,
            "precio": $scope.precio
        }
        $http.post('/tiendavirtual/idproducto', data).then(function(data){
            //success
            $scope.nombre_producto = data.data.nombre_producto;
            $scope.cantidad = data.data.cantidad;
            $scope.precio = data.data.precio;
        },function(){
            //error
            $scope.nombre_producto = "";
            $scope.cantidad = "";
            $scope.precio = "";
            $scope.filas = [];
        });
        
    };
    
    //acción del botón actualizar
    $scope.guardarProducto = function(){
        data = {
            "nombre_producto":$scope.nombre_producto,
            "fecha_compra": $scope.fecha_compra,
            "procedencia": $scope.procedencia,
            "precio": $scope.precio,
            "cantidad": $scope.fecha_vencimiento,
            "fecha_vencimiento": $scope.fecha_vencimiento,
            "tipo_almacenamiento": $scope.tipo_almacenamiento,
        };
        $http.post('/cajero/estudiante', data).then(function(data){
            //success
            $scope.nombre_producto = data.data.nombre_producto;
            $scope.fecha_compra = data.data.fecha_compra;
            $scope.procedencia = data.data.procedencia;
            $scope.precio= data.data.precio;
            $scope.cantidad = data.data.cantidad;
            $scope.fecha_vencimiento = data.data.fecha_vencimiento;
            $scope.tipo_almacenamiento = data.data.tipoalmacenamiento;
        },function(){
            //error
            $scope.nombre_producto = "";
            $scope.fecha_compra = "";
            $scope.procedencia = "";
            $scope.precio= 0;
            $scope.cantidad = 0;
            $scope.fecha_vencimiento = "";
            $scope.tipo_almacenamiento = "";
            $scope.filas = [];
        });
        
    };
    
    // acción del botón borrar
    $scope.borrar = function(){
        if($scope.id_producto == undefined || $scope.id_producto == null){
            $scope.id_producto = 0;
        }
        data = {
            "id_producto": $scope.id_producto
        }
        $http.delete('/cajero/eliminarproducto/'+$scope.id_producto, data).then(function(data){
            alert("El producto ha sido eliminado");
            
        },function(){
            alert("Ha ocurrido un error");
        });
    };
});


